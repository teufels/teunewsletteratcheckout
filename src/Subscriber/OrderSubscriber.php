<?php

namespace TeuNewsletterAtCheckout\Subscriber;

use Shopware\Core\Checkout\Customer\Exception\CustomerNotFoundException;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Content\Newsletter\SalesChannel\AbstractNewsletterSubscribeRoute;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\Checkout\Confirm\CheckoutConfirmPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TeuNewsletterAtCheckout\Service\SalesChannelService;

class OrderSubscriber implements EventSubscriberInterface
{
    /** @var EntityRepository$orderRepository */
    protected EntityRepository $orderRepository;

    /** @var AbstractNewsletterSubscribeRoute $abstractNewsletterSubscribeRoute */
    protected AbstractNewsletterSubscribeRoute $abstractNewsletterSubscribeRoute;

    /** @var SystemConfigService $systemConfigService */
    protected SystemConfigService $systemConfigService;

    /** @var EntityRepository $customerRepository */
    protected EntityRepository $customerRepository;

    public function __construct(
        EntityRepository $orderRepository,
        AbstractNewsletterSubscribeRoute $abstractNewsletterSubscribeRoute,
        private readonly SalesChannelService $salesChannelService,
        SystemConfigService $systemConfigService,
        EntityRepository $customerRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->abstractNewsletterSubscribeRoute = $abstractNewsletterSubscribeRoute;
        $this->systemConfigService = $systemConfigService;
        $this->customerRepository = $customerRepository;
    }

    public static function getSubscribedEvents() : array
    {
        return [
            OrderEvents::ORDER_WRITTEN_EVENT => 'onOrderWritten',
            CheckoutConfirmPageLoadedEvent::class  => 'onConfirmPageLoaded',
        ];
    }

    public function onOrderWritten(EntityWrittenEvent $entityWrittenEvent)
    {
        if (isset($_REQUEST['teu-checkout-newsletter']) && $_REQUEST['teu-checkout-newsletter'] === 'on') {
            $order = $this->getOrder($entityWrittenEvent);
            $context = $entityWrittenEvent->getContext();
            $this->subscribeUserToNewsletter($order, $context);
        }
    }

    public function onConfirmPageLoaded(CheckoutConfirmPageLoadedEvent $event)
    {
        $CustomerHasNewsletter = !empty($event->getSalesChannelContext()->getCustomer()->getNewsletterSalesChannelIds());

        if (!$CustomerHasNewsletter) {
            $event->getPage()->assign(['showNewsletter' => true]);
            $event->getPage()->addArrayExtension('teuNewsletter', ['show' => true]);
        }
        //print_r($CustomerhasNewsletter);die;
    }

    private function subscribeUserToNewsletter(OrderEntity $order, $context)
    {
        $orderCustomer = $order->getOrderCustomer();
        $subscribeMethod = $this->systemConfigService->get('TeuNewsletterAtCheckout.config.subscribeMethod');
        $customer = $this->getCustomer($orderCustomer->getCustomerId(), $context);
        $customerAddress =  $customer->getAddresses()->first();


        $dataBag = new RequestDataBag([
            'email' => $orderCustomer->getEmail(),
            'option' => $subscribeMethod,
            'storefrontUrl' => '',
            'title' => $orderCustomer->getTitle(),
            'firstName'=> $orderCustomer->getFirstName(),
            'lastName'=> $orderCustomer->getLastName(),
            'zipCode' => $customerAddress->getZipcode(),
            'city' =>  $customerAddress->getCity(),
            'street' => $customerAddress->getStreet(),
            'salutationId'=> $orderCustomer->getSalutationId(),
            'languageId' => $customer->getLanguageId(),
            'customFields' => $orderCustomer->getCustomFields()

        ]);


        $salesChannelContext = $this->salesChannelService->createSalesChannelContext($order->getSalesChannelId(),$order->getLanguageId());
        $this->abstractNewsletterSubscribeRoute->subscribe($dataBag, $salesChannelContext, false);
        //$this->setCustomerNewsletterFlag($customer, $context);
    }

    private function setCustomerNewsletterFlag($customer, $context)
    {
        $customer->setNewsletter(true);

        $this->customerRepository->update(
            [
                ['id' => $customer->getId(), 'newsletter' => true],
            ],
            $context);
    }

    private function getOrder($entityWrittenEvent)
    {
        $context = $entityWrittenEvent->getContext();
        $payloads = $entityWrittenEvent->getPayloads();
        $orderCriteria = new Criteria(['id' => $payloads[0]['id']]);
        $order = $this->orderRepository->search($orderCriteria, $context)->first();
        return $order;
    }

    private function getCustomer($customerId, $context)
    {
        $criteria = new Criteria([$customerId]);
        $criteria->addAssociation('addresses');
        $customer = $this->customerRepository->search($criteria, $context)->first();
        if($customer === null){
            throw new CustomerNotFoundException(json_encode($criteria));
        }
        return $customer;
    }




}
